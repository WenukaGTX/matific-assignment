import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'matific-assignment';

  constructor() { }
}

export class SidenavAutosizeExample {
  showFiller = false;
}

export class DialogContentExampleDialog { }