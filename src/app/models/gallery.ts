export class Gallery {
    title: string;
    thumbnail_url: string;
    episode_url: string;
}