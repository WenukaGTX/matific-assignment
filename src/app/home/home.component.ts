import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Gallery } from '../models/gallery';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public galleryItems: Gallery[];

  constructor(private service: HomeService) { }

  ngOnInit(): void {
    this.service.getGalleryItems().subscribe((response: Gallery[]) => {
      this.galleryItems = response;
    })
  }

}
