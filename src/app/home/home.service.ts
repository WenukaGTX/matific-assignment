import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  public getGalleryItems() {
    return this.http.get('https://ljifg6p8cd.execute-api.us-east-1.amazonaws.com/production/matific-test-gallery-activities').pipe(take(1))
  }
}
