import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VideoModalComponent } from '../video-modal/video-modal.component'

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BannerComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  openDialog() {
    const dialogRef = this.dialog.open(VideoModalComponent, {
      panelClass: 'video-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Modal closed`);
    });
  }

  ngOnInit(): void {
  }

}
