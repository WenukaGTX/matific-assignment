import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-video-modal',
  templateUrl: './video-modal.component.html',
  styleUrls: ['./video-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VideoModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
